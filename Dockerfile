#==================== Building Stage ================================================

# Create the image based on the official Node 8.9.0 image from Dockerhub
FROM node:8.9.0 as node

# Create a directory where our app will be placed. This might not be necessary
RUN mkdir -p /todo-html

# Change directory so that our commands run inside this new directory
WORKDIR /todo-html

# Get all the code needed to run the app
COPY . /todo-html

# Expose the port the app runs in
EXPOSE 8080

#==================== Setting up stage ====================
# Create image based on the official nginx - Alpine image
FROM nginx:1.13.7-alpine

COPY --from=node /todo-html/src/ /usr/share/nginx/html

COPY ./nginx-to-do-app.conf /etc/nginx/conf.d/default.conf


